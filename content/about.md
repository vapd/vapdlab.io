---
title: "About"
date: 2017-08-20T21:38:52+08:00
lastmod: 2017-08-28T21:41:52+08:00
menu: "main"
weight: 50

# you can close something for this content if you open it in config.toml.
comment: false
mathjax: false
---

"VAPD" stands for [value-added products division](http://bigrsdata.radi.ac.cn/) from the Aerospace Information Research Institute (AIR), Chinese Academy of Sciences (CAS).


A number of value-added products from remote sensing have been released by AIR and VAPD, including:

* [Ready-To-Use (RTU) product](http://ids.ceode.ac.cn/rtu/)
* [Data share plan of Satellite data](http://ids.ceode.ac.cn/query.html)

<!-- Learn more and contribute on [GitHub](http://www.aircas.ac.cn/). -->