---
title: "30 m Resolution Surface Water Map across China of 2015"
author: "Vapd"
date: 2018-07-05T10:08:58+08:00
tags: ["inland water", "SWMC", "remote sensing"]
---

A 30 m resolution Surface Water Map across China of 2015 (SWMC-2015) is released for free download.
***

## Description
Surface waters account for a very small proportion of total water reserves and they are critical for supporting biodiversity, ecological security, drinking water, agricultural irrigation and industrial production. Multilayer perceptron (MLP) neural network was first introduced to produce a surface water map across China in 2015 (SWMC-2015). 

The metadata information of SWMC-2015 is as follows:

Info | Value
--- | ---
Datum | WGS1984
Projection | Albers Equal Area
Central meridian | 110
Standard parallel-1 | 25
Standard parallel-2 | 47
Resolution | 30m
Covering(top) | 5865892.10
Covering(bottom) | 1865618.44
Covering(left) | -3034380.20
Covering(right) | 1848649.38

Pixel attributes:

Layer | Attribute | DataType | Notes
--- | --- | --- | ---
1   | Class | Byte | Possible values:<br> &#8226; 0 (zero): nodata <br> &#8226; 1: water <br> &#8226; 2: nowater


<figure>
  <a href="/image/SWMC.jpg" target="_blank">
    <img src="/image/SWMC.jpg" alt="SWMC 2015"/>
  </a>
  <figcaption><em>SWMC 2015</em></figcaption>
</figure>

## View

View SWMC 2015 in [map](/html/view_SWMC2015.html).

## Download
SWMC-2015 can be downloaded from the [ftp server1](ftp://124.16.184.141) or [ftp server2](ftp://10.7.2.2).

## Citation
To use this dataset, please consider citing the following papers.

```
@article{jiang2018multilayer,
  title={Multilayer Perceptron Neural Network for Surface Water Extraction in Landsat 8 OLI Satellite Images},
  author={Jiang, Wei and He, Guojin and Long, Tengfei and Ni, Yuan and Liu, Huichan and Peng, Yan and Lv, Kenan and Wang, Guizhou},
  journal={Remote Sensing},
  volume={10},
  number={5},
  pages={755},
  year={2018},
  publisher={Multidisciplinary Digital Publishing Institute}
}

@article{jiang2018multilayer,
  title={Surface Water Map across China in 2015 (SWMC-2015) Derived from Landsat 8 OLI Satellite Images},
  author={Jiang, Wei and He, Guojin and Ni, Yuan and Long, Tengfei and Guo, Hongxiang and Liu, Huichan and Peng, Yan},
  journal={Water},
  publisher={Multidisciplinary Digital Publishing Institute}
}
```