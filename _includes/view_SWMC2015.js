var client_ip;
var getClientIP = function(){
    // do some asynchronous work
    // and when the asynchronous stuff is complete
    return $.getJSON("https://jsonip.com?callback=?", function(data) {
        // alert("Your IP address is :- " + data.ip);
        client_ip = data.ip;
    });
};
var deferred = getClientIP();
$.when(deferred).done(function() {

    var data_ip = "124.16.184.141";
    if(client_ip == "124.16.186.16"){
        data_ip = "10.7.2.2";
    }

    var OSM_layer = new ol.layer.Tile({
                    title: 'OSM',
                    // type: 'base',
                    visible: true,
                    source: new ol.source.OSM()
                });
    var attribution = new ol.Attribution({
        html: 'Tiles &copy; <a href="http://services.arcgisonline.com/ArcGIS/' +
            'rest/services/World_Imagery/MapServer">ArcGIS</a>'
    });
    var arcgis_layer = new ol.layer.Tile({
        title: 'ArcGIS World Imagery',
        visible: false,
        source: new ol.source.XYZ({
            attributions: [attribution],
            url: 'http://server.arcgisonline.com/ArcGIS/rest/services/' +
                'World_Imagery/MapServer/tile/{z}/{y}/{x}'
        })
        });
    var SWMC2015_layer = new ol.layer.Image({
                        title: "SWMC 2015",
                        source: new ol.source.ImageWMS({
                            url: 'http://'+data_ip+':5122/SWMC2015wms',
                            params: {
                                    'LAYERS': 'SWMC2015',
                                    'SERVICE': 'WMS',
                                    'VERSION':'1.3.0',
                                    'REQUEST': 'GetMap',
                                    'FORMAT': "image/png",
                                    'TRANSPARENT': 'true',
                                },
                            serverType: 'mapserver'
                        })
                    });

    var map = new ol.Map({
    layers: [
    arcgis_layer, 
    OSM_layer, 
    SWMC2015_layer],
    target: 'map',
    maxResolution: 30,
    projection: "EPSG:3857",
    view: new ol.View({
        center: [11155164.993,4383863.040],
        extent:[7934136.302, 1647520.469, 15094766.653, 7047432.002],
        zoom: 10,
        minZoom: 5,
        maxZoom: 13,
    })
});

var layerSwitcher = new ol.control.LayerSwitcher({
    tipLabel: 'Layers'
});
map.addControl(layerSwitcher);
    });