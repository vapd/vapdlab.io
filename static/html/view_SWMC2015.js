(function() {
    var client_ip;
    var getClientIP = function(){
        return $.getJSON("https://jsonip.com?callback=?", function(data) {
            client_ip = data.ip;
        });
    };
    // client_ip = "223.16.186.16";
    var deferred = getClientIP();
    $.when(deferred).done(function() 
        {

            var swmc2015_url = 'http://124.16.184.141:5122/SWMC2015wms';
            if(client_ip == "124.16.186.16"){
                swmc2015_url = 'http://10.7.2.2:5122/SWMC2015wms';
            }

    // var swmc2015_url = "http://124.16.184.141:5122/SWMC2015wms";
    
    var OSM_layer = new ol.layer.Tile({
                    title: 'OSM',
                    type: 'base',
                    visible: true,
                    source: new ol.source.OSM()
                });
    var attribution = new ol.Attribution({
        html: 'Tiles &copy; <a href="http://services.arcgisonline.com/ArcGIS/' +
            'rest/services/World_Imagery/MapServer">ArcGIS</a>'
    });
    var arcgis_world_layer = new ol.layer.Tile({
        title: 'ArcGIS World Imagery',
        type: 'base',
        visible: false,
        source: new ol.source.XYZ({
            attributions: [attribution],
            url: 'http://server.arcgisonline.com/ArcGIS/rest/services/' +
                'World_Imagery/MapServer/tile/{z}/{y}/{x}'
        })
        });
    var arcgis_landsat_layer = new ol.layer.Tile({
        title: 'ArcGIS Landsat8 Views',
        type: 'base',
        visible: false,
        source: new ol.source.TileArcGISRest({
            url: 'https://landsat2.arcgis.com/arcgis/rest/services/Landsat8_Views/ImageServer',
        })
        });
    var SWMC2015_layer = new ol.layer.Image({
        title: 'SWMC-2015',
        //   extent: extent,
          source: new ol.source.ImageWMS({
            url: swmc2015_url,
            params: {
                'LAYERS': 'SWMC2015',
                'SERVICE': 'WMS',
                'VERSION':'1.1.0',
                'REQUEST': 'GetMap',
                'FORMAT': "image/png",
            },
            serverType: 'mapserver'
          })
        });

    var map = new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Group({
                'title': 'Base maps',
                layers: [
                    arcgis_world_layer, 
                    arcgis_landsat_layer,
                    OSM_layer, 
                //     new ol.layer.Tile({
                //         title: 'Water color',
                //         type: 'base',
                //         visible: false,
                //         source: new ol.source.Stamen({
                //             layer: 'watercolor'
                //         })
                //     }),
                //     new ol.layer.Tile({
                //         title: 'OSM',
                //         type: 'base',
                //         visible: true,
                //         source: new ol.source.OSM()
                //     }),
                //     new ol.layer.Group({
                //         title: 'Satellite and labels',
                //         type: 'base',
                //         combine: true,
                //         visible: false,
                //         layers: [
                //             new ol.layer.Tile({
                //                 source: new ol.source.BingMaps({
                //                     // Get your own key at https://www.bingmapsportal.com/
                //                     key: 'Ahd_32h3fT3C7xFHrqhpKzoixGJGHvOlcvXWy6k2RRYARRsrfu7KDctzDT2ei9xB',
                //                     imagerySet: 'Aerial'
                //                 })
                //             }),
                //             new ol.layer.Tile({
                //                 source: new ol.source.Stamen({
                //                     layer: 'terrain-labels'
                //                 })
                //             })
                //         ]
                //     })
                ]
            }),
            new ol.layer.Group({
                title: 'Overlays',
                layers: [SWMC2015_layer,
                    // new ol.layer.Tile({
                    //     title: 'Countries',
                    //     source: new ol.source.TileWMS({
                    //         url: 'http://demo.opengeo.org/geoserver/wms',
                    //         params: {'LAYERS': 'ne:ne_10m_admin_1_states_provinces_lines_shp'},
                    //         serverType: 'geoserver'
                    //     })
                    // })
                ]
            })
        ],
        maxResolution: 30,
        projection: "EPSG:3857",
        view: new ol.View({
            center: [11155164.993,4383863.040],
            extent:[7934136.302, 1647520.469, 15094766.653, 7047432.002],
            zoom: 4,
            minZoom: 4,
            maxZoom: 13,
        })
        // view: new ol.View({
        //     center: ol.proj.transform([-0.92, 52.96], 'EPSG:4326', 'EPSG:3857'),
        //     zoom: 6
        // })
    });

    // LayerSwitcher

    var layerSwitcher = new ol.control.LayerSwitcher({
        tipLabel: 'Layers' // Optional label for button
    });
    map.addControl(layerSwitcher);

    // // Popup
    // var popup = new ol.Overlay.Popup();
    // map.addOverlay(popup);

    // map.on('singleclick', function(evt) {
    //     var prettyCoord = ol.coordinate.toStringHDMS(ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326'), 2);
    //     popup.show(evt.coordinate, '<div><h2>Coordinates</h2><p>' + prettyCoord + '</p></div>');
    // });
});

})();