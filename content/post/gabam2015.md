---
title: "30 m resolution global annual burned area map of 2015"
author: "Vapd"
date: 2018-05-06T14:22:56+08:00
tags: ["burned area", "GABAM", "remote sensing", "Earth Engine"]
---

This page is deprecated, please move to the [updated GABAM](https://vapd.gitlab.io/post/gabam).
