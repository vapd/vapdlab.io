---
title: "Updated 30 m resolution global annual burned area map"
author: "Vapd"
date: 2021-04-06T14:22:56+08:00
tags: ["burned area", "GABAM", "remote sensing", "Earth Engine"]
---

30 m resolution global annual burned area maps (GABAM) of 1985-2021 are released for free download.

***

## Description

The annual burned area map is defined as **spatial extent of fires that occurs within a whole year and not of fires that occurred in previous years**.

GABAM was generated via an automated pipeline based on Google Earth Engine (GEE), using all the available Landsat images on GEE platform. The product was projected in a Geographic (Lat/Long) projection at 0.00025&deg; (approximately 30 meters) resolution, with the WGS84 horizontal datum and the EGM96 vertical datum, consisting of 10&deg;&times;10&deg; tiles spanning the range 180&deg;W–180&deg;E and 80&deg;N–60&deg;S. 

The following figure illustrates the global distribution of burned area (BA) density from GABAM 2015, which is defined as the proportion of burned pixels in a 0.25&deg;&times;0.25&deg; grid.

<figure>
  <a href="/image/gabam_grid.jpg" target="_blank">
    <img src="/image/gabam_grid.jpg" alt="GABAM 2015 grid"/>
  </a>
  <figcaption><em>BA density distribution from GABAM 2015</em></figcaption>
</figure>

Comparison with the recent Fire_cci version 5.0[^footnote2] global BA product of 250 m resolution (highest spatial resolution before GABAM):

<figure>
  <a href="/image/fcci_grid.jpg" target="_blank">
    <img src="/image/fcci_grid.jpg" alt="Fire_cci 2015 grid"/>
  </a>
  <figcaption><em>BA density distribution from Fire_cci 2015</em></figcaption>
</figure>

<a href="/image/compare.jpg">![compare](/image/compare.jpg)</a>

## Download

GABAM can be downloaded from [ftp server1](ftp://124.16.184.141/GABAM), [ftp server2](ftp://10.7.2.2/GABAM) or [Zenodo](https://zenodo.org/records/13858799).


It is recommended to use FTP clients, such as [Filezilla](https://filezilla-project.org/download.php?type=client), to download data from FTP servers.

## Citation

To use this dataset, please cite the following paper.

    @article{Long_2019,
    doi = {10.3390/rs11050489},
    url = {https://doi.org/10.3390%2Frs11050489},
    year = 2019,
    month = {feb},
    publisher = {{MDPI} {AG}},
    volume = {11},
    number = {5},
    pages = {489},
    author = {Tengfei Long and Zhaoming Zhang and Guojin He and Weili Jiao and Chao Tang and Bingfang Wu and Xiaomei Zhang and Guizhou Wang and Ranyu Yin},
    title = {30 m Resolution Global Annual Burned Area Mapping Based on Landsat Images and Google Earth Engine},
    journal = {Remote Sensing}
    }

- [Long, T.; Zhang, Z.; He, G.; Jiao, W.; Tang, C.; Wu, B.; Zhang, X.; Wang, G.; Yin, R. 30 m Resolution Global Annual Burned Area Mapping Based on Landsat Images and Google Earth Engine. Remote Sens. 2019, 11, 489.](https://www.mdpi.com/2072-4292/11/5/489)

Please feel free to contact us (<longtf@radi.ac.cn>), feedback is welcome! 

[^footnote]: This research has been supported by The National Key Research and Development Program of China (2016YFA0600302 and 2016YFB0501502).
[^footnote2]: Chuvieco, E., Lizundia-Loiola, J., Pettinari, M.L., Ramo, R., Padilla, M., Tansey, K., Mouillot, F., Laurent, P., Storm, T., Heil, A., & Plummer, S. (2018). Generation and analysis of a new global burned area product based on MODIS 250 m reflectance bands and thermal anomalies. Earth Systems Science Data, 10, 2015-2031.